﻿using Microsoft.EntityFrameworkCore;
using product.Data;
using product.Interfaces;
using product.Models;
using product.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace product.Repository
{
    public class ProductRepository : IProductRepository
    {

        private readonly ProductContext _context;

        public ProductRepository(ProductContext context)
        {
            _context = context;
        }

        public Product Create(Product product)
        {
            _context.Products.Add(product);
            _context.SaveChanges();
            return product;
        }

        public Product Delete(Product product)
        {
            _context.Products.Attach(product);
            _context.Entry(product).State = EntityState.Deleted;
            _context.SaveChanges();
            return product;
        }

        public Product Edit(Product product)
        {
            _context.Products.Attach(product);
            _context.Entry(product).State = EntityState.Modified;
            _context.SaveChanges();
            return product;
        }

       

        public List<Product> Getitems()
        {
            List<Product> products = _context.Products.ToList();
            return products;
        }

        public Product GetProduct(int id)
        {
            Product product = _context.Products.Where(u => u.ProductID == id).FirstOrDefault();
            return product;
        }

        
    }
}
