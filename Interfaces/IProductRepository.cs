﻿using product.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace product.Interfaces

{
    public interface IProductRepository
    {
        List<Product> Getitems();
        Product GetProduct(int id);
        Product Create(Product product);
        Product Edit(Product product);
        Product Delete(Product product);
    }
}
