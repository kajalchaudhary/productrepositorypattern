﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using product.Data;
using product.Models;
using product.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace product.Controllers
{
    public class ProductController : Controller
    {
        public IActionResult Index()
        {
            List<Product> products = _productRepo.Getitems();
            return View(products);
        }
       

        private readonly IProductRepository _productRepo;
        public ProductController(IProductRepository productrepo)
        {
            
            _productRepo = productrepo;
        }

        public IActionResult Create()
        {
            Product product = new Product();
            return View(product);
        }

        [HttpPost]
        public IActionResult Create(Product product)
        {
            try
            {
                product = _productRepo.Create(product);
            }
            catch(Exception e)
            {
                Console.WriteLine("{0} Exception caught.", e);
            }
            return RedirectToAction(nameof(Index));
        }
        
        public IActionResult Details(int id)
        {
            Product product = _productRepo.GetProduct(id);
            return View(product);
        }
       

        public IActionResult Edit(int id)
        {
            Product product = _productRepo.GetProduct(id);
            return View(product);
        }

        [HttpPost]
        public IActionResult Edit(Product product)
        {
            try
            {
                product = _productRepo.Edit(product);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Exception caught.", e);
            }
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Delete(int id)
        {
            Product product = _productRepo.GetProduct(id);
            return View(product);
        }

        [HttpPost]
        public IActionResult Delete(Product product)
        {
            try
            {
                product = _productRepo.Delete(product);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Exception caught.", e);
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
